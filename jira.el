;;; jira.el --- Utilities for fetching information from Jira -*- lexical-binding: t; -*-

;; Copyright (C) 2020 Thomas Stenersen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Thomas Stenersen <stenersen.thomas@gmail.com>
;; Version: 0.1
;; Package-Requires: ((emacs "25.1") (request "0.3.0") (s "1.12.0") (popup "0.5.3"))
;; Keywords: tools, jira
;; URL: https://gitlab.com/thomsten/jira.el

;;; Commentary:

;; This package provides an interface to Atlassian Jira through Emacs. It gives
;; an easy way to fetch informations about issue keys found during code
;; browsing.

;;; Code:

(require 'request)
(require 'org)
(require 'json)
(require 'popup)
(require 's)

(defgroup jira nil
  "Jira group."
  :group 'tools)

(defcustom jira-url
  "https://jira.atlassian.com"
  "URL to Jira server."
  :group 'jira
  :type 'string)

(defcustom jira-url-requires-authorization
  'nil
  "Set t if `jira-url' requires authorization."
  :group 'jira
  :type 'boolean)

(defcustom jira-use-popup-tip
  'nil
  "Set t to use a popup-tip for displaying the issue."
  :group 'jira
  :type 'boolean)

(defcustom jira-convert-with-pandoc
  'nil
  "Set t to use pandoc for converting Jira markup to Org syntax."
  :group 'jira
  :type 'boolean)

(defvar jira--auth-encoded
  nil
  "Key used to fetch user password from cache.")

(defun jira--get-api (name)
  "Get the REST API endpoint for NAME."
  (concat jira-url "/rest/api/2/" name))

(defun jira--get-issue-url (issue-key)
  "Get URL to a given ISSUE-KEY."
  (concat jira-url "/browse/" issue-key))

(defun jira--convert-to-org (str)
  "Filter STR for invalid characters."
  (with-temp-buffer
    (insert (decode-coding-string str 'utf-8))
    (if jira-convert-with-pandoc
        (shell-command-on-region (point-min) (point-max) "pandoc -f textile -t org"))
    (buffer-string)))

(cl-defun jira--get-issue-on-success (&key data &allow-other-keys)
  "On success we get ARGS."
  (let* ((fields (plist-get data :fields))
         (issue-key (plist-get data :key))
         (title (decode-coding-string (plist-get fields :summary) 'utf-8))
         (status (plist-get fields :status))
         (comments (plist-get (plist-get fields :comment) :comments))
         (description (jira--convert-to-org (plist-get fields :description))))
    (if jira-use-popup-tip
        (popup-tip (format "%s: %s [%s]\n%s" issue-key title (plist-get status :name) description))
      (with-current-buffer-window
       (format "*%s*" issue-key) nil nil
       (insert (format "* %s: %s :%s:\n%s\n\n" issue-key title (plist-get status :name) description))
       (insert (mapconcat (lambda (comment)
                            (concat "** "
                                    (plist-get (plist-get comment :author) :emailAddress)
                                    " @ "
                                    (plist-get comment :updated)
                                    "\n"
                                    (jira--convert-to-org (plist-get comment :body))))
                          comments
                          "\n\n"))
       (org-mode)
       (outline-show-all)))))

(cl-defun jira--on-error (&key response &allow-other-keys)
  "Error callback will be given RESPONSE."
  (setq jira--auth-encoded nil)
  (message "Error requesting data: %S"
           (plist-get (request-response-data response) :errorMessages)))

(defun jira--get-auth ()
  "Get authentication header."
  (if jira--auth-encoded
      jira--auth-encoded

    (setq jira--auth-encoded
          (concat "Basic "
                  (base64-encode-string
                   (concat (read-from-minibuffer "Username: ") ":"
                           (password-read "Password: ")))))))

(defun jira--request (url type params success-callback)
  "Send an HTTP request to the Jira REST api.
- URL is the request url
- TYPE is either `GET' or `POST'
- PARAMS is a list of cons cells
- SUCCESS-CALLBACK is the callback called when the request returns successfuly"

  (let ((headers '(("Content-Type" . "application/json"))))

    (if  jira-url-requires-authorization
        (push `("Authorization" . ,(jira--get-auth)) headers))

    (request url
      :type  `,type
      :params  `,params
      :headers `,headers
      :parser (lambda ()
                (let ((json-object-type 'plist))
                  (json-read)))
      :success `,success-callback
      :error 'jira--on-error)))

(cl-defun jira--search-on-success (&key data &allow-other-keys)
  (message "Results:\n%s" (mapconcat
                        (lambda (d)
                          (concat (plist-get d :key) ": "
                                  (plist-get (plist-get d :fields) :summary)))
                        (plist-get data :issues)
                        "\n")))

(defun jira-search (jql)
  "Search Jira with JQL query."
  (interactive "sJQL: ")
  (let ((url (concat (jira--get-api "search"))))
    (jira--request
     url
     "GET"
     `(("jql" . ,jql)
       ("maxResults" . 5)
       ("fields" . "summary"))
     'jira--search-on-success)))

(defun jira--request-issue (issue)
  "Get info about ISSUE."

  (let ((url (concat (jira--get-api "issue") "/" issue)))
    (jira--request
     url
     "GET"
     '(("fields" . "description")
       ("fields" . "summary")
       ("fields" . "status")
       ("fields" . "comment"))
     'jira--get-issue-on-success)))

(defun jira--is-issue-key (issue)
  "Return t if ISSUE is on a valid form."
  (s-matches-p "[A-Z]+-[0-9]+" issue))

(defun jira--get-issue-at-point ()
  "Get the Jira issue at point."
  (let* ((beginning-of-issue (save-excursion
                               (search-backward-regexp "\\([^A-Z0-9-]\\|^\\)")))
         (end-of-issue (save-excursion
                         (search-forward-regexp "\\([^A-Z0-9-]\\|$\\)"))))
    (when (and beginning-of-issue end-of-issue)
      (if (not (or (= (point-min) beginning-of-issue)
           (s-matches-p "[A-Z]"
                        (buffer-substring-no-properties
                         beginning-of-issue (1+ beginning-of-issue)))))
          (setq beginning-of-issue (1+ beginning-of-issue)))
      (if (not (or (= (point-max) end-of-issue)
                   (s-matches-p "[0-9]"
                                (buffer-substring-no-properties
                                 (1- end-of-issue) end-of-issue))))
          (setq end-of-issue (1- end-of-issue)))
      (s-trim (buffer-substring-no-properties
                           beginning-of-issue end-of-issue)))))


(defun jira--browse-url (issue)
  "Open URL to ISSUE in default browser."
  (browse-url (jira--get-issue-url issue)))

(defun jira--apply-to-issue-at-point (func)
  "Apply FUNC to issue at point."
  (let ((issue (jira--get-issue-at-point)))
      (when (and issue (jira--is-issue-key issue))
        (apply func (list issue)))))

(defun jira--apply-to-issue (func issue)
  "Apply FUNC to ISSUE."
  (when (and issue (jira--is-issue-key issue))
    (apply func (list issue))))

(defun jira-issue-at-point ()
  "Get the Jira issue at point."
  (interactive)
  (jira--apply-to-issue-at-point 'jira--request-issue))

(defun jira-get-issue (issue)
  "Return information about ISSUE."
  (interactive "sIssue key: ")
  (jira--apply-to-issue 'jira--request-issue issue))

(defun jira-browse-issue-at-point ()
  "Open the issue at point using `browse-url'."
  (interactive)
  (jira--apply-to-issue-at-point 'jira--browse-url))

(defun jira-browse-issue (issue)
  "Browse ISSUE in default browser."
  (interactive "sIssue key: ")
  (jira--apply-to-issue 'jira--browse-url issue))

(provide 'jira)
;;; jira.el ends here
