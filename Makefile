##
# jira.el
#
# @file
# @version 0.1

CASK        ?= cask
EMACS       ?= emacs
DIST        ?= dist
EMACSFLAGS   = --batch -Q
EMACSBATCH   = $(EMACS) $(EMACSFLAGS)

VERSION     := $(shell EMACS=$(EMACS) $(CASK) version)
PKG_DIR     := $(shell EMACS=$(EMACS) $(CASK) package-directory)
PROJ_ROOT   := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

EMACS_D      = ~/.emacs.d
USER_ELPA_D  = $(EMACS_D)/elpa

SRCS         = $(filter-out %-pkg.el, $(wildcard *.el))
TESTS        = $(wildcard test/*.el)
TAR          = $(DIST)/jira-$(VERSION).tar


.PHONY: help all check test lint deps install uninstall reinstall clean-all clean clean-elc
help:								## Display this help section
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_-]+:.*?## / {printf "\033[36m%-38s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
.DEFAULT_GOAL := help

all: deps $(TAR)					## Run all targets

deps:								## Install dependencies
	$(CASK) install

install: $(TAR)						## Install the package
	$(EMACSBATCH) -l package -f package-initialize \
	--eval '(package-install-file "$(PROJ_ROOT)/$(TAR)")'

uninstall:							## Uninstall the package
	rm -rf $(USER_ELPA_D)/jira-*

reinstall: clean uninstall install	## Re-install a fresh version of the package

clean-all: clean					## Clean up everything, including cask dependencies
	rm -rf $(PKG_DIR)

clean-elc:							## Remove the byte-compiled files
	rm -f *.elc

clean: clean-elc					## Clean build
	rm -rf $(DIST)
	rm -f *-pkg.el

$(TAR): $(DIST) $(SRCS)
	$(CASK) package $(DIST)

$(DIST):
	mkdir $(DIST)

check: test lint					## Run tests and lint

test: $(PKG_DIR)					## Run unit tests
	${CASK} exec ert-runner

lint: $(SRCS) clean-elc				## Run lint
	# Byte compile all and stop on any warning or error
	${CASK} emacs $(EMACSFLAGS) \
	--eval "(setq byte-compile-error-on-warn t)" \
	-L . -f batch-byte-compile ${SRCS} ${TESTS}

	# Run package-lint to check for packaging mistakes
	${CASK} emacs $(EMACSFLAGS) \
	--eval "(require 'package)" \
	--eval "(push '(\"melpa\" . \"http://melpa.org/packages/\") package-archives)" \
	--eval "(package-initialize)" \
	--eval "(package-refresh-contents)" \
	-l package-lint.el \
	-f package-lint-batch-and-exit ${SRCS}

# end
