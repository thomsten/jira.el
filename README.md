# jira.el

The `jira.el` package adds support for fetching information from Atlassian Jira
-- straight into your Emacs!

![Live preview](img/jira-get-issue.gif)

## Requirements

The following packages are required (available on MELPA):
- `request.el`
- `popup.el`
- `s.el`
- `pandoc`, unless `jira-convert-with-pandoc` is set to `nil`

## Usage

Use `customize-variable [RET] jira-url` to set the `jira-url` to your Jira
server. Furthermore, set the `jira-url-requires-authorization` to `t` if your
Jira server requires login information.

You can use `customize-variable` to set `jira-use-popup-tip` to `t` in order
to display the issue title and description in a pop-up tip.

Finally, use `jira-issue-at-point` to fetch the information about the given
issue around point, `jira-get-issue` to look up an arbitrary issue key or the
accompanying `jira-browse-*` functions to open the issues in your
`default-browser`.

### Authentication

`jira.el` will ask you for authentication the first time requesting issue
information. The authentication data will stored base64-encoded in a variable
and cleared if any request fails.
